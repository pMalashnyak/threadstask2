package com.Malashnyak.threads2.MyLock;

import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Test {
  private static ReadWriteLock lock = new ReadWriteLock();
  private int A = 0;

  class MyThread implements Runnable {

    @Override
    public void run() {
      try {
        lock.lockRead();
        System.out.println(
            LocalDateTime.now().getSecond() + " " + Thread.currentThread().getName() + " A=" + A);
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      } finally {
        lock.unlockRead();
      }
    }
  }

  public void show() {
    ExecutorService executor = Executors.newFixedThreadPool(4);
    Runnable rT1 = new MyThread();
    Runnable rT2 = new MyThread();
    Runnable rT3 = new MyThread();
    executor.submit(rT1);
    executor.submit(rT2);
    executor.submit(rT3);
    executor.submit(
        () -> {
          try {
            lock.lockWrite();

            A = 25;
            System.out.println(
                LocalDateTime.now().getSecond()
                    + " "
                    + Thread.currentThread().getName()
                    + " A="
                    + A);
            Thread.sleep(5000);
          } catch (InterruptedException e) {
          } finally {
            try {
              lock.unlockWrite();
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
        });
    executor.submit(rT1);
    executor.submit(rT2);
    executor.submit(rT3);
    executor.shutdown();
  }
}
