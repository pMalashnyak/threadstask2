package com.Malashnyak.threads2.Locks;

import java.util.concurrent.locks.ReentrantLock;

public class Locks implements Runnable {
  private Integer integer;
  private ReentrantLock lock = new ReentrantLock();

  public Locks(Integer _i) {
    integer = _i;
  }

  private void method1() {
    lock.lock();
    try {
      for (int i = 1; i < 5; i++) {
        integer++;
        System.out.printf("%s %d Method1\n", Thread.currentThread().getName(), integer);
        Thread.sleep(100);
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    } finally {
      lock.unlock();
    }
  }

  private void method2() {
    lock.lock();
    try {
      for (int i = 1; i < 5; i++) {
        integer++;
        System.out.printf("%s %d Method2\n", Thread.currentThread().getName(), integer);

        Thread.sleep(100);
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    } finally {
      lock.unlock();
    }
  }

  private void method3() {
    lock.lock();
    try {
      for (int i = 1; i < 5; i++) {
        integer++;
        System.out.printf("%s %d Method3\n", Thread.currentThread().getName(), integer);
        Thread.sleep(100);
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    } finally {
      lock.unlock();
    }
  }

  public void run() {
    method1();
    method2();
    method3();
  }
}
